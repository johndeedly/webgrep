using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using Microsoft.Playwright;
using toolbelt;

namespace webgrep
{
    public interface IElementWrapper
    {
        IEnumerable<IElementWrapper> AttachedElements(string selector);
        bool ClickOn(float? timeout = 5000);
        bool DblClickOn(float? timeout = 5000);
        bool ScrollIntoViewIfNeeded(float? timeout = 5000);
        string GetFormattedText();
        string GetText();
        string GetInnerHtml();
        string GetOuterHtml();
        string GetElementHtml();
        string GetAttribute(string name);
        bool SetAttribute(string name, string value);
        bool SetFileUpload(string path, float? timeout = 5000);
        bool SetFileUploads(IEnumerable<string> path, float? timeout = 5000);
        bool TakeScreenshot(Stream stream, float? timeout = 5000);
        bool SaveScreenshot(string path, float? timeout = 5000);
        bool WaitUntilAttached(string selector, float? timeout = 5000);
        bool WaitUntilDetached(string selector, float? timeout = 5000);
        JsonElement? Evaluate(string expression, params object[] arguments);
    }

    public class ElementWrapper : IElementWrapper
    {
        IElementHandle element;
        IFrameWrapper frame;
        
        public ElementWrapper(IElementHandle element, IFrameWrapper frame)
        {
            this.element = element;
            this.frame = frame;
        }

        public IEnumerable<IElementWrapper> AttachedElements(string selector)
        {
            try
            {
                return element.QuerySelectorAllAsync(selector)
                    .Concat(x => x.Select(y => new ElementWrapper(y, frame)))
                    .Await();
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return Enumerable.Empty<IElementWrapper>();
            }
        }

        public bool ClickOn(float? timeout = 5000)
        {
            try
            {
                element.ClickAsync(new ElementHandleClickOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return false;
            }
        }

        public bool DblClickOn(float? timeout = 5000)
        {
            try
            {
                element.DblClickAsync(new ElementHandleDblClickOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return false;
            }
        }

        public bool ScrollIntoViewIfNeeded(float? timeout = 5000)
        {
            try
            {
                element.ScrollIntoViewIfNeededAsync(new ElementHandleScrollIntoViewIfNeededOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return false;
            }
        }

        public string GetFormattedText()
        {
            try
            {
                return element.InnerTextAsync().Concat(x => x.Trim()).Await();
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return string.Empty;
            }
        }

        public string GetText()
        {
            try
            {
                return element.TextContentAsync().Concat(x => x.Trim()).Await();
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return string.Empty;
            }
        }

        public string GetInnerHtml()
        {
            try
            {
                // remove newlines, then whitespace before, between and after tags in this order
                var elem = element.EvaluateAsync("(e) => e.innerHTML.replace(/\\n/g,'').replace(/[\\t ]+\\</g,'<').replace(/\\>[\\t ]+\\</g,'><').replace(/\\>[\\t ]+/g,'>')").Await();
                if (elem.HasValue && elem.Value.ValueKind == JsonValueKind.String)
                    return elem.Value.GetString();
                return null;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return string.Empty;
            }
        }

        public string GetOuterHtml()
        {
            try
            {
                // remove newlines, then whitespace before, between and after tags in this order
                var elem = element.EvaluateAsync("(e) => e.outerHTML.replace(/\\n/g,'').replace(/[\t ]+\\</g,'<').replace(/\\>[\t ]+\\</g,'><').replace(/\\>[\t ]+/g,'>')").Await();
                if (elem.HasValue && elem.Value.ValueKind == JsonValueKind.String)
                    return elem.Value.GetString();
                return null;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return string.Empty;
            }
        }

        public string GetElementHtml()
        {
            try
            {
                var elem = element.EvaluateAsync("(e) => e.cloneNode(false).outerHTML").Await();
                if (elem.HasValue && elem.Value.ValueKind == JsonValueKind.String)
                    return elem.Value.GetString();
                return null;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return string.Empty;
            }
        }

        public string GetAttribute(string name)
        {
            try
            {
                return element.GetAttributeAsync(name).Concat(x => x.Trim()).Await();
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return string.Empty;
            }
        }

        public bool SetAttribute(string name, string value)
        {
            try
            {
                element.EvaluateAsync($"(e,[name,val]) => e.setAttribute(name, val)", new[] { name, value }).Await();
                return true;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return false;
            }
        }

        public bool SetFileUpload(string path, float? timeout = 5000)
        {
            try
            {
                element.SetInputFilesAsync(path, new ElementHandleSetInputFilesOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return false;
            }
        }

        public bool SetFileUploads(IEnumerable<string> paths, float? timeout = 5000)
        {
            try
            {
                element.SetInputFilesAsync(paths, new ElementHandleSetInputFilesOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return false;
            }
        }

        public bool TakeScreenshot(Stream stream, float? timeout = 5000)
        {
            try
            {
                element.ScreenshotAsync(new ElementHandleScreenshotOptions
                {
                    Timeout = timeout
                }).Concat(data => stream.Write(data, 0, data.Length)).Await();
                return true;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return false;
            }
        }

        public bool SaveScreenshot(string path, float? timeout = 5000)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            {
                return TakeScreenshot(fs, timeout);
            }
        }

        public bool WaitUntilAttached(string selector, float? timeout = 5000)
        {
            try
            {
                element.WaitForSelectorAsync(selector, new ElementHandleWaitForSelectorOptions
                {
                    State = WaitForSelectorState.Attached,
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return false;
            }
        }

        public bool WaitUntilDetached(string selector, float? timeout = 5000)
        {
            try
            {
                element.WaitForSelectorAsync(selector, new ElementHandleWaitForSelectorOptions
                {
                    State = WaitForSelectorState.Detached,
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return false;
            }
        }

        public JsonElement? Evaluate(string expression, params object[] arguments)
        {
            try
            {
                JsonElement? elem = element.EvaluateAsync(expression, arguments).Await();
                return elem;
            }
            catch (Exception ex)
            {
                frame.ErrorLog.Add(ex.Message);
                frame.LastError = ex;
                return null;
            }
        }
    }
}