using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsCommon.Close, "Page")]
    [OutputType(typeof(bool))]
    public class ClosePage : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        protected override void ProcessRecord()
        {
            if (Page != null)
            {
                bool res = Page.ClosePage();
                WriteObject(res);
            }
            if (Browser != null)
            {
                bool res = Page.ClosePage();
                WriteObject(res);
            }
        }
    }
}
