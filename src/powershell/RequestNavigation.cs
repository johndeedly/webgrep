using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsLifecycle.Request, "Navigation")]
    [OutputType(typeof(bool))]
    public class RequestNavigation : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Mandatory = true, ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(Mandatory = true, ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Url { get; set; }

        [Parameter(ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        public SwitchParameter NoWait { get; set; }

        [Parameter(ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        public Nullable<float> Timeout { get; set; }

        protected override void ProcessRecord()
        {
            if (Page != null)
            {
                bool res = NoWait ? Page.NavigateToNoWait(Url, Timeout) : Page.NavigateTo(Url, Timeout);
                WriteObject(res);
            }
            if (Browser != null)
            {
                bool res = NoWait ? Browser.NavigateToNoWait(Url, Timeout) : Browser.NavigateTo(Url, Timeout);
                WriteObject(res);
            }
        }
    }
}
