using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsLifecycle.Start, "Browser")]
    [OutputType(typeof(IBrowserInstance))]
    public class StartBrowser : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Firefox", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        public SwitchParameter Firefox { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Chromium", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        public SwitchParameter Chromium { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Webkit", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        public SwitchParameter Webkit { get; set; }

        [Parameter(ParameterSetName = "Firefox", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Chromium", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Webkit", ValueFromPipelineByPropertyName = true)]
        public SwitchParameter Headless { get; set; }

        protected override void ProcessRecord()
        {
            bool nosel = false;
            IBrowserInstance browser = null;
            if (!Firefox && !Chromium && !Webkit)
            {
                nosel = true;
                WriteVerbose("No preference selected. Default browser is used.");
            }
            try
            {
                if (nosel || Firefox)
                {
                    WriteVerbose("Starting firefox instance.");
                    browser = new FirefoxInstance(new BrowserLaunchOptions {
                        Headless = Headless
                    });
                }
                if (Chromium)
                {
                    WriteVerbose("Starting chromium instance.");
                    browser = new ChromiumInstance(new BrowserLaunchOptions {
                        Headless = Headless
                    });
                }
                if (Webkit)
                {
                    WriteVerbose("Starting webkit instance.");
                    browser = new WebkitInstance(new BrowserLaunchOptions {
                        Headless = Headless
                    });
                }
            }
            catch (Exception ex)
            {
                WriteError(new ErrorRecord(
                     ex,
                     "StartBrowser",
                     ErrorCategory.OpenError,
                     "browser"));
            }
            WriteObject(browser);
        }
    }
}
