using System;
using System.IO;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsData.Save, "Screenshot")]
    [OutputType(typeof(bool))]
    public class SaveScreenshot : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "BrowserPath", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "BrowserStream", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "PagePath", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "PageStream", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "ElementPath", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "ElementStream", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IElementWrapper Element { get; set; }

        [Parameter(ParameterSetName = "BrowserStream", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "PageStream", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "ElementStream", ValueFromPipelineByPropertyName = true)]
        public Stream Stream { get; set; }

        [Parameter(ParameterSetName = "BrowserPath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "PagePath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "ElementPath", ValueFromPipelineByPropertyName = true)]
        public string Path { get; set; }

        [Parameter(ParameterSetName = "BrowserPath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "PagePath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "ElementPath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "BrowserStream", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "PageStream", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "ElementStream", ValueFromPipelineByPropertyName = true)]
        public Nullable<float> Timeout { get; set; }

        protected override void ProcessRecord()
        {
            if (Element != null)
            {
                var res = Stream != null ? Element.TakeScreenshot(Stream, Timeout) : Element.SaveScreenshot(Path, Timeout);
                WriteObject(res);
            }
            if (Page != null)
            {
                var res = Stream != null ? Page.TakeScreenshot(Stream, Timeout) : Page.SaveScreenshot(Path, Timeout);
                WriteObject(res);
            }
            if (Browser != null)
            {
                var res = Stream != null ? Browser.TakeScreenshot(Stream, Timeout) : Browser.SaveScreenshot(Path, Timeout);
                WriteObject(res);
            }
        }
    }
}
