using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsCommon.Find, "AttachedElements")]
    [OutputType(typeof(IElementWrapper))]
    public class FindAttachedElements : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Frame", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IFrameWrapper Frame { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Element", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IElementWrapper Element { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Frame", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Element", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Selector { get; set; }

        protected override void ProcessRecord()
        {
            if (Element != null)
            {
                var elements = Element.AttachedElements(Selector);
                WriteObject(elements, true);
            }
            if (Frame != null)
            {
                var elements = Frame.AttachedElements(Selector);
                WriteObject(elements, true);
            }
            if (Page != null)
            {
                var elements = Page.AttachedElements(Selector);
                WriteObject(elements, true);
            }
            if (Browser != null)
            {
                var elements = Browser.AttachedElements(Selector);
                WriteObject(elements, true);
            }
        }
    }
}
