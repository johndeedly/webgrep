using System;
using System.Management.Automation;
using System.Text.Json;

namespace webgrep.powershell
{
    [Cmdlet(VerbsLifecycle.Invoke, "Javascript")]
    [OutputType(typeof(Nullable<JsonElement>))]
    public class InvokeJavascript : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Element", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IElementWrapper Element { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Element", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Expression { get; set; }

        [Parameter(ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Element", ValueFromPipelineByPropertyName = true)]
        public string[] Arguments { get; set; }

        protected override void ProcessRecord()
        {
            if (Element != null)
            {
                var res = Element.Evaluate(Expression, Arguments);
                WriteObject(res);
            }
            if (Page != null)
            {
                var res = Page.Evaluate(Expression, Arguments);
                WriteObject(res);
            }
            if (Browser != null)
            {
                var res = Browser.Evaluate(Expression, Arguments);
                WriteObject(res);
            }
        }
    }
}
