using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsCommon.Get, "Pages")]
    [OutputType(typeof(IPageWrapper))]
    public class GetPages : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        protected override void ProcessRecord()
        {
            WriteObject(Browser.Pages, true);
        }
    }
}
