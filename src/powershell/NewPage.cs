using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsCommon.New, "Page")]
    [OutputType(typeof(IPageWrapper))]
    public class NewPage : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        protected override void ProcessRecord()
        {
            WriteObject(Browser.NewPage());
        }
    }
}
