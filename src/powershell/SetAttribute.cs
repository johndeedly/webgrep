using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsCommon.Set, "Attribute")]
    [OutputType(typeof(bool))]
    public class SetAttribute : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IElementWrapper Element { get; set; }

        [Parameter(Position = 1, Mandatory = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Name { get; set; }

        [Parameter(Position = 2, Mandatory = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Value { get; set; }

        protected override void ProcessRecord()
        {
            if (Element != null)
            {
                var res = Element.SetAttribute(Name, Value);
                WriteObject(res);
            }
        }
    }
}
