using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsLifecycle.Stop, "Browser")]
    [OutputType(typeof(bool))]
    public class StopBrowser : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        protected override void ProcessRecord()
        {
            try
            {
                Browser.Dispose();
                Browser = null;
                WriteObject(true);
            }
            catch (Exception)
            {
                WriteObject(false);
            }
        }
    }
}
