using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsCommon.Set, "FileUpload")]
    [OutputType(typeof(bool))]
    public class SetFileUpload : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "BrowserPath", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "BrowserPaths", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "PagePath", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "PagePaths", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "FramePath", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "FramePaths", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IFrameWrapper Frame { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "ElementPath", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "ElementPaths", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IElementWrapper Element { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "BrowserPath", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "PagePath", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "FramePath", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "BrowserPaths", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "PagePaths", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "FramePaths", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Selector { get; set; }

        [Parameter(ParameterSetName = "BrowserPath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "PagePath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "FramePath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "ElementPath", ValueFromPipelineByPropertyName = true)]
        public string Path { get; set; }

        [Parameter(ParameterSetName = "BrowserPaths", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "PagePaths", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "FramePaths", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "ElementPaths", ValueFromPipelineByPropertyName = true)]
        public string[] Paths { get; set; }

        [Parameter(ParameterSetName = "BrowserPath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "PagePath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "FramePath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "ElementPath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "BrowserPaths", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "PagePaths", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "FramePaths", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "ElementPaths", ValueFromPipelineByPropertyName = true)]
        public Nullable<float> Timeout { get; set; }

        protected override void ProcessRecord()
        {
            if (Element != null)
            {
                var res = Paths != null ? Element.SetFileUploads(Paths, Timeout) : Element.SetFileUpload(Path, Timeout);
                WriteObject(res);
            }
            if (Frame != null)
            {
                var res = Paths != null ? Frame.SetFileUploads(Selector, Paths, Timeout) : Frame.SetFileUpload(Selector, Path, Timeout);
                WriteObject(res);
            }
            if (Page != null)
            {
                var res = Paths != null ? Page.SetFileUploads(Selector, Paths, Timeout) : Page.SetFileUpload(Selector, Path, Timeout);
                WriteObject(res);
            }
            if (Browser != null)
            {
                var res = Paths != null ? Browser.SetFileUploads(Selector, Paths, Timeout) : Browser.SetFileUpload(Selector, Path, Timeout);
                WriteObject(res);
            }
        }
    }
}
