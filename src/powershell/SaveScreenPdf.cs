using System.IO;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsData.Save, "ScreenPdf")]
    [OutputType(typeof(bool))]
    public class SaveScreenPdf : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "BrowserPath", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "BrowserStream", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "PagePath", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "PageStream", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(ParameterSetName = "BrowserStream", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "PageStream", ValueFromPipelineByPropertyName = true)]
        public Stream Stream { get; set; }

        [Parameter(ParameterSetName = "BrowserPath", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "PagePath", ValueFromPipelineByPropertyName = true)]
        public string Path { get; set; }

        protected override void ProcessRecord()
        {
            if (Page != null)
            {
                var res = Stream != null ? Page.TakeScreenPdf(Stream) : Page.SaveScreenPdf(Path);
                WriteObject(res);
            }
            if (Browser != null)
            {
                var res = Stream != null ? Browser.TakeScreenPdf(Stream) : Browser.SaveScreenPdf(Path);
                WriteObject(res);
            }
        }
    }
}
