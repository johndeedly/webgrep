using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsCommon.Get, "Attribute")]
    [OutputType(typeof(string))]
    public class GetAttribute : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IElementWrapper Element { get; set; }

        [Parameter(Position = 1, Mandatory = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Name { get; set; }

        protected override void ProcessRecord()
        {
            if (Element != null)
            {
                var value = Element.GetAttribute(Name);
                WriteObject(value);
            }
        }
    }
}
