using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsCommon.Get, "Text")]
    [OutputType(typeof(string))]
    public class GetText : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Frame", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IFrameWrapper Frame { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Element", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IElementWrapper Element { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Frame", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Selector { get; set; }

        [Parameter(ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Frame", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Element", ValueFromPipelineByPropertyName = true)]
        public SwitchParameter Formatted { get; set; }

        [Parameter(ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Frame", ValueFromPipelineByPropertyName = true)]
        public Nullable<float> Timeout { get; set; }

        protected override void ProcessRecord()
        {
            if (Element != null)
            {
                string res = Formatted ? Element.GetFormattedText() : Element.GetText();
                WriteObject(res);
            }
            if (Frame != null)
            {
                string res = Formatted ? Frame.GetFormattedText(Selector, Timeout) : Frame.GetText(Selector, Timeout);
                WriteObject(res);
            }
            if (Page != null)
            {
                string res = Formatted ? Page.GetFormattedText(Selector, Timeout) : Page.GetText(Selector, Timeout);
                WriteObject(res);
            }
            if (Browser != null)
            {
                string res = Formatted ? Browser.GetFormattedText(Selector, Timeout) : Browser.GetText(Selector, Timeout);
                WriteObject(res);
            }
        }
    }
}
