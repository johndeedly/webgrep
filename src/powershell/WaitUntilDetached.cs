using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsLifecycle.Wait, "UntilDetached")]
    [OutputType(typeof(bool))]
    public class WaitUntilDetached : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Frame", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IFrameWrapper Frame { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Element", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IElementWrapper Element { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Frame", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Element", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public string Selector { get; set; }

        [Parameter(ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Frame", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Element", ValueFromPipelineByPropertyName = true)]
        public Nullable<float> Timeout { get; set; }

        protected override void ProcessRecord()
        {
            if (Element != null)
            {
                var res = Element.WaitUntilDetached(Selector, Timeout);
                WriteObject(res);
            }
            if (Frame != null)
            {
                var res = Frame.WaitUntilDetached(Selector, Timeout);
                WriteObject(res);
            }
            if (Page != null)
            {
                var res = Page.WaitUntilDetached(Selector, Timeout);
                WriteObject(res);
            }
            if (Browser != null)
            {
                var res = Browser.WaitUntilDetached(Selector, Timeout);
                WriteObject(res);
            }
        }
    }
}
