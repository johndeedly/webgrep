using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsData.Save, "DownloadToPath")]
    [OutputType(typeof(bool))]
    public class SaveDownloadToPath : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Path { get; set; }

        [Parameter(Position = 2, Mandatory = true, ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 2, Mandatory = true, ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public ScriptBlock ActionsToStartDownload { get; set; }

        [Parameter(ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        public Nullable<float> Timeout { get; set; }

        protected override void ProcessRecord()
        {
            if (Page != null)
            {
                Action act = delegate() { ActionsToStartDownload.Invoke(); };
                var res = Page.DownloadSaveToPath(Path, act, Timeout);
                WriteObject(res);
            }
            if (Browser != null)
            {
                Action act = delegate() { ActionsToStartDownload.Invoke(); };
                var res = Browser.DownloadSaveToPath(Path, act, Timeout);
                WriteObject(res);
            }
        }
    }
}
