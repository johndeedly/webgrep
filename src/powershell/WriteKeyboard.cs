using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsCommunications.Write, "Keyboard")]
    public class WriteKeyboard : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "BrowserText", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "BrowserKey", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "PageText", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "PageKey", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "BrowserText", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "PageText", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Text { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "BrowserKey", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "PageKey", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Key { get; set; }

        protected override void ProcessRecord()
        {
            bool success = false;
            if (Page != null)
            {
                if (!string.IsNullOrEmpty(Text))
                    success = Page.KeyboardTypeText(Text);
                if (!string.IsNullOrEmpty(Key))
                    success = Page.KeyboardPressKey(Key);
            }
            if (Browser != null)
            {
                if (!string.IsNullOrEmpty(Text))
                    success = Browser.KeyboardTypeText(Text);
                if (!string.IsNullOrEmpty(Key))
                    success = Browser.KeyboardPressKey(Key);
            }
            if (!success && !string.IsNullOrEmpty(Text))
                WriteWarning($"Could not write {Text} to keyboard.");
            if (!success && !string.IsNullOrEmpty(Key))
                WriteWarning($"Could not send keyboard key {Key}.");
        }
    }
}
