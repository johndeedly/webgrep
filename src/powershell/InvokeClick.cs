using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsLifecycle.Invoke, "Click")]
    [OutputType(typeof(bool))]
    public class InvokeClick : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Frame", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IFrameWrapper Frame { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Element", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IElementWrapper Element { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Frame", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Selector { get; set; }

        [Parameter(ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Frame", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Element", ValueFromPipelineByPropertyName = true)]
        public Nullable<float> Timeout { get; set; }

        [Parameter(ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Frame", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Element", ValueFromPipelineByPropertyName = true)]
        public SwitchParameter Doubleclick { get; set; }

        protected override void ProcessRecord()
        {
            if (Element != null)
            {
                var res = Doubleclick ? Element.DblClickOn(Timeout) : Element.ClickOn(Timeout);
                WriteObject(res);
            }
            if (Frame != null)
            {
                var res = Doubleclick ? Frame.DblClickOn(Selector, Timeout) : Frame.ClickOn(Selector, Timeout);
                WriteObject(res);
            }
            if (Page != null)
            {
                var res = Doubleclick ? Page.DblClickOn(Selector, Timeout) : Page.ClickOn(Selector, Timeout);
                WriteObject(res);
            }
            if (Browser != null)
            {
                var res = Doubleclick ? Browser.DblClickOn(Selector, Timeout) : Browser.ClickOn(Selector, Timeout);
                WriteObject(res);
            }
        }
    }
}
