using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsLifecycle.Wait, "DomContentLoaded")]
    [OutputType(typeof(bool))]
    public class WaitDomContentLoaded : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Frame", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IFrameWrapper Frame { get; set; }

        [Parameter(ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(ParameterSetName = "Frame", ValueFromPipelineByPropertyName = true)]
        public Nullable<float> Timeout { get; set; }

        protected override void ProcessRecord()
        {
            if (Frame != null)
            {
                var res = Frame.WaitDomContentLoaded(Timeout);
                WriteObject(res);
            }
            if (Page != null)
            {
                var res = Page.WaitDomContentLoaded(Timeout);
                WriteObject(res);
            }
            if (Browser != null)
            {
                var res = Browser.WaitDomContentLoaded(Timeout);
                WriteObject(res);
            }
        }
    }
}
