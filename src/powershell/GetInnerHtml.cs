using System;
using System.Management.Automation;

namespace webgrep.powershell
{
    [Cmdlet(VerbsCommon.Get, "InnerHtml")]
    [OutputType(typeof(string))]
    public class GetInnerHtml : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Browser", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IBrowserInstance Browser { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Page", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IPageWrapper Page { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Frame", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IFrameWrapper Frame { get; set; }

        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Element", ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        [ValidateNotNull]
        public IElementWrapper Element { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Browser", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Page", ValueFromPipelineByPropertyName = true)]
        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Frame", ValueFromPipelineByPropertyName = true)]
        [ValidateNotNullOrEmpty]
        public string Selector { get; set; }

        protected override void ProcessRecord()
        {
            if (Element != null)
            {
                string res = Element.GetInnerHtml();
                WriteObject(res);
            }
            if (Frame != null)
            {
                string res = Frame.GetInnerHtml(Selector);
                WriteObject(res);
            }
            if (Page != null)
            {
                string res = Page.GetInnerHtml(Selector);
                WriteObject(res);
            }
            if (Browser != null)
            {
                string res = Browser.GetInnerHtml(Selector);
                WriteObject(res);
            }
        }
    }
}
