using Microsoft.Playwright;
using toolbelt;

namespace webgrep
{
    public sealed class FirefoxInstance : BrowserInstance
    {
        public FirefoxInstance(BrowserLaunchOptions options = null)
        {
            BrowserTypeLaunchOptions opts = null;
            if (options != null)
            {
                opts = new BrowserTypeLaunchOptions();
                if (options.Headless != null)
                    opts.Headless = options.Headless;
                if (options.ProxyServer != null)
                    opts.Proxy = new Proxy
                    {
                        Server = options.ProxyServer
                    };
            }
            BrowserNewContextOptions ctxopts = null;
            if (options?.AcceptDownloads != null)
            {
                ctxopts = new BrowserNewContextOptions()
                {
                    AcceptDownloads = options.AcceptDownloads
                };
            }
            browser = playwright.Firefox.LaunchAsync(opts).Await();
            context = browser.NewContextAsync(ctxopts).Await();
            NewPage();
        }
    }
}