using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Playwright;
using toolbelt;

namespace webgrep
{
    public interface IPageWrapper : IFrameWrapper
    {
        IReadOnlyList<IFrameWrapper> Frames { get; }

        bool ClosePage();
        bool KeyboardPressKey(string key);
        bool KeyboardTypeText(string text);
        bool NavigateTo(string url, float? timeout = 5000);
        bool NavigateToNoWait(string url, float? timeout = 5000);
        bool Route(string path, Action<IRouteWrapper> handler);
        bool Route(Func<string, bool> path, Action<IRouteWrapper> handler);
        bool Unroute(string path);
        bool Unroute(Func<string, bool> path);
        bool TakeScreenshot(Stream stream, float? timeout = 5000);
        bool SaveScreenshot(string path, float? timeout = 5000);
        bool TakeScreenPdf(Stream stream);
        bool SaveScreenPdf(string path);
        bool TakePrintPdf(Stream stream);
        bool SavePrintPdf(string path);
        string DownloadSaveToPath(string path, Action actionsToStartDownload, float? timeout = 5000);
        JsonElement? Evaluate(string expression, params object[] arguments);
    }

    public class PageWrapper : IPageWrapper
    {
        protected IBrowserInstance instance;
        protected IPage page;
        public IList<string> ErrorLog => instance.ErrorLog;
        public IDictionary<string, Regex> routeCache;

        public Exception LastError
        {
            get => instance.LastError;
            set => instance.LastError = value;
        }

        public IReadOnlyList<IFrameWrapper> Frames
        {
            get
            {
                return page.Frames.Select(x => new FrameWrapper(x, this)).ToArray();
            }
        }

        public PageWrapper(IPage page, IBrowserInstance instance)
        {
            this.page = page;
            this.instance = instance;
            this.routeCache = new Dictionary<string, Regex>();
        }

        public bool ClosePage()
        {
            try
            {
                page.CloseAsync().Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool NavigateTo(string url, float? timeout = 5000)
        {
            try
            {
                try
                {
                    page.GotoAsync(url, new PageGotoOptions
                    {
                        WaitUntil = WaitUntilState.NetworkIdle,
                        Timeout = timeout
                    }).Await();
                }
                catch (TimeoutException ex)
                {
                    ErrorLog.Add(ex.Message);
                    LastError = ex;
                    page.WaitForLoadStateAsync(LoadState.DOMContentLoaded, new PageWaitForLoadStateOptions
                    {
                        Timeout = timeout
                    }).Await();
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool NavigateToNoWait(string url, float? timeout = 5000)
        {
            try
            {
                page.GotoAsync(url, new PageGotoOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool Route(string path, Action<IRouteWrapper> handler)
        {
            try
            {
                Regex rex;
                if (!routeCache.TryGetValue(path, out rex))
                {
                    rex = new Regex(path, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    routeCache[path] = rex;
                }
                page.RouteAsync(rex, (r) => handler(new RouteWrapper(r, this))).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool Route(Func<string, bool> path, Action<IRouteWrapper> handler)
        {
            try
            {
                page.RouteAsync(path, (r) => handler(new RouteWrapper(r, this))).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool Unroute(string path)
        {
            try
            {
                Regex rex;
                if (routeCache.TryGetValue(path, out rex))
                {
                    page.UnrouteAsync(rex).Await();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool Unroute(Func<string, bool> path)
        {
            try
            {
                page.UnrouteAsync(path).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool TakeScreenshot(Stream stream, float? timeout = 5000)
        {
            try
            {
                page.ScreenshotAsync(new PageScreenshotOptions
                {
                    FullPage = true,
                    Timeout = timeout
                }).Concat(data => stream.Write(data, 0, data.Length)).Await();
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }

        public bool SaveScreenshot(string path, float? timeout = 5000)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            {
                return TakeScreenshot(fs, timeout);
            }
        }

        public bool TakeScreenPdf(Stream stream)
        {
            try
            {
                if (instance is not ChromiumInstance)
                    throw new InvalidOperationException("PDF creation is currently only supported in chromium like browsers");
                page.EmulateMediaAsync(new PageEmulateMediaOptions
                {
                    Media = Media.Screen
                }).Await();
                page.PdfAsync(new PagePdfOptions
                {
                    Format = "A3"
                }).Concat(data => stream.Write(data, 0, data.Length)).Await();
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }
        
        public bool SaveScreenPdf(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            {
                return TakeScreenPdf(fs);
            }
        }

        public bool TakePrintPdf(Stream stream)
        {
            try
            {
                if (instance is not ChromiumInstance)
                    throw new InvalidOperationException("PDF creation is currently only supported in chromium like browsers");
                page.EmulateMediaAsync(new PageEmulateMediaOptions
                {
                    Media = Media.Print
                }).Await();
                page.PdfAsync(new PagePdfOptions
                {
                    Format = "A3"
                }).Concat(data => stream.Write(data, 0, data.Length)).Await();
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }
        
        public bool SavePrintPdf(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            {
                return TakePrintPdf(fs);
            }
        }

        public bool KeyboardTypeText(string text)
        {
            try
            {
                page.Keyboard.TypeAsync(text).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool KeyboardPressKey(string key)
        {
            try
            {
                page.Keyboard.PressAsync(key).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool SetFileUpload(string selector, string path, float? timeout = 5000)
        {
            return new FrameWrapper(page.MainFrame, this)
                .SetFileUpload(selector, path, timeout);
        }

        public bool SetFileUploads(string selector, IEnumerable<string> paths, float? timeout = 5000)
        {
            return new FrameWrapper(page.MainFrame, this)
                .SetFileUploads(selector, paths, timeout);
        }

        public string DownloadSaveToPath(string path, Action actionsToStartDownload, float? timeout = 5000)
        {
            try
            {
                if (path == null || string.IsNullOrWhiteSpace(path))
                    path = Directory.GetCurrentDirectory();
                else if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                Task<IDownload> downloadTask = page.WaitForDownloadAsync(new PageWaitForDownloadOptions
                {
                    Timeout = timeout
                });
                actionsToStartDownload();
                IDownload download = downloadTask.Await();
                string combined = Path.Combine(path, download.SuggestedFilename);
                download.SaveAsAsync(combined).Await();
                return combined;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                path = string.Empty;
                return string.Empty;
            }
        }

        public JsonElement? Evaluate(string expression, params object[] arguments)
        {
            try
            {
                JsonElement? elem = page.EvaluateAsync(expression, arguments).Await();
                return elem;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return null;
            }
        }

        public IEnumerable<IElementWrapper> AttachedElements(string selector)
        {
            return new FrameWrapper(page.MainFrame, this)
                .AttachedElements(selector);
        }

        public bool ClickOn(string selector, float? timeout = 5000)
        {
            return new FrameWrapper(page.MainFrame, this)
                .ClickOn(selector, timeout);
        }

        public bool DblClickOn(string selector, float? timeout = 5000)
        {
            return new FrameWrapper(page.MainFrame, this)
                .DblClickOn(selector, timeout);
        }

        public bool ScrollIntoViewIfNeeded(string selector, float? timeout = 5000)
        {
            return new FrameWrapper(page.MainFrame, this)
                .ScrollIntoViewIfNeeded(selector, timeout);
        }

        public bool WaitUntilAttached(string selector, float? timeout = 5000)
        {
            return new FrameWrapper(page.MainFrame, this)
                .WaitUntilAttached(selector, timeout);
        }

        public bool WaitUntilDetached(string selector, float? timeout = 5000)
        {
            return new FrameWrapper(page.MainFrame, this)
                .WaitUntilDetached(selector, timeout);
        }

        public string GetFormattedText(string selector, float? timeout = 5000)
        {
            return new FrameWrapper(page.MainFrame, this)
                .GetFormattedText(selector, timeout);
        }

        public string GetText(string selector, float? timeout = 5000)
        {
            return new FrameWrapper(page.MainFrame, this)
                .GetText(selector, timeout);
        }

        public string GetInnerHtml(string selector)
        {
            return new FrameWrapper(page.MainFrame, this)
                .GetInnerHtml(selector);
        }

        public string GetOuterHtml(string selector)
        {
            return new FrameWrapper(page.MainFrame, this)
                .GetOuterHtml(selector);
        }

        public string GetElementHtml(string selector)
        {
            return new FrameWrapper(page.MainFrame, this)
                .GetElementHtml(selector);
        }

        public bool WaitDomContentLoaded(float? timeout = 5000)
        {
            return new FrameWrapper(page.MainFrame, this)
                .WaitDomContentLoaded(timeout);
        }
    }
}