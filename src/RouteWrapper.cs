using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Playwright;
using toolbelt;

namespace webgrep
{
    public interface IRouteWrapper
    {
        IRequestWrapper Request { get; }
        IList<HeaderWrapper> Headers { get; }
        void AddHeader(string name, string value);
        void AddHeader(HeaderWrapper header);
        void AddHeaderRange(IEnumerable<HeaderWrapper> headers);
        bool RemoveHeader(string name, string value);
        bool RemoveHeader(HeaderWrapper header);
        bool RemoveHeaderRange(IEnumerable<HeaderWrapper> headers);
        bool Abort();
        bool Continue();
        bool ContinueToUrl(string url);
        bool ContinueWithPostData(byte[] postData);
        bool FulfillWithBody(string body, string contentType = null, int status = 200);
        bool FulfillWithBytes(byte[] bodyBytes, string contentType = null, int status = 200);
        bool FulfillWithPath(string filePath, string contentType = null, int status = 200);
    }

    public class RouteWrapper : IRouteWrapper
    {
        IRoute route;
        IPageWrapper instance;
        IList<HeaderWrapper> headers;
        static readonly object _sync = new object();

        public IRequestWrapper Request
        {
            get
            {
                return new RequestWrapper(route.Request, instance);
            }
        }

        public IList<HeaderWrapper> Headers
        {
            get
            {
                return headers;
            }
        }

        public RouteWrapper(IRoute route, IPageWrapper instance)
        {
            this.route = route;
            this.instance = instance;
            this.headers = new List<HeaderWrapper>();
        }

        public void AddHeader(string name, string value)
        {
            AddHeader(new HeaderWrapper(new Header { Name = name, Value = value }));
        }

        public void AddHeader(HeaderWrapper header)
        {
            headers.Add(header);
        }

        public void AddHeaderRange(IEnumerable<HeaderWrapper> headers)
        {
            foreach (var elem in headers)
                this.headers.Add(elem);
        }

        public bool RemoveHeader(string name, string value)
        {
            return RemoveHeader(new HeaderWrapper(new Header { Name = name, Value = value }));
        }

        public bool RemoveHeader(HeaderWrapper header)
        {
            return headers.Remove(header);
        }

        public bool RemoveHeaderRange(IEnumerable<HeaderWrapper> headers)
        {
            bool res = true;
            foreach (var elem in headers)
                res |= this.headers.Remove(elem);
            return res;
        }

        public bool Abort()
        {
            try
            {
                // never await here!!
                route.AbortAsync();
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }

        public bool Continue()
        {
            try
            {
                // never await here!!
                route.ContinueAsync(new RouteContinueOptions
                {
                    Headers = headers.Any() ? headers.Select(x => new KeyValuePair<string, string>(x.Name, x.Value)) : null
                });
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }

        public bool ContinueToUrl(string url)
        {
            try
            {
                // never await here!!
                route.ContinueAsync(new RouteContinueOptions
                {
                    Url = url,
                    Headers = headers.Any() ? headers.Select(x => new KeyValuePair<string, string>(x.Name, x.Value)) : null
                });
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }

        public bool ContinueWithPostData(byte[] postData)
        {
            try
            {
                // never await here!!
                route.ContinueAsync(new RouteContinueOptions
                {
                    PostData = postData,
                    Headers = headers.Any() ? headers.Select(x => new KeyValuePair<string, string>(x.Name, x.Value)) : null
                });
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }

        public bool Fetch(Action<IResponseWrapper> actionsOnResponse)
        {
            try
            {
                route.FetchAsync().Concat(response =>
                {
                    lock(_sync)
                    {
                        actionsOnResponse(new ResponseWrapper(response, instance));
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }

        public bool FulfillWithBody(string body, string contentType = null, int status = -1)
        {
            try
            {
                // never await here!!
                route.FulfillAsync(new RouteFulfillOptions
                {
                    Body = body,
                    Headers = headers.Any() ? headers.Select(x => new KeyValuePair<string, string>(x.Name, x.Value)) : null,
                    ContentType = contentType,
                    Status = status < 0 ? null : status
                });
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }

        public bool FulfillWithBytes(byte[] bodyBytes, string contentType = null, int status = -1)
        {
            try
            {
                // never await here!!
                route.FulfillAsync(new RouteFulfillOptions
                {
                    BodyBytes = bodyBytes,
                    Headers = headers.Any() ? headers.Select(x => new KeyValuePair<string, string>(x.Name, x.Value)) : null,
                    ContentType = contentType,
                    Status = status < 0 ? null : status
                });
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }

        public bool FulfillWithPath(string path, string contentType = null, int status = -1)
        {
            try
            {
                // never await here!!
                route.FulfillAsync(new RouteFulfillOptions
                {
                    Path = path,
                    Headers = headers.Any() ? headers.Select(x => new KeyValuePair<string, string>(x.Name, x.Value)) : null,
                    ContentType = contentType,
                    Status = status < 0 ? null : status
                });
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }
    }
}