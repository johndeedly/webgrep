using System;
using System.Linq;
using System.Text;
using Terminal.Gui;
using toolbelt;

namespace webgrep
{
    public class TerminalUserInterfacePython
    {
        IScriptingWrapper python;
        IBrowserInstance browser;
        StringBuilder sb;
        bool errored;

        public TerminalUserInterfacePython(IScriptingWrapper python, IBrowserInstance browser)
        {
            this.python = python;
            this.browser = browser;
            this.sb = new StringBuilder();
            python.RedirectStreams = true;
            python.OutputDataReceived += (str) =>
            {
                sb.AppendLine(str);
            };
            python.ErrorDataReceived += (t, str) =>
            {
                sb.AppendLine($"{Enum.GetName<ScriptingErrorType>(t)}: {str}");
                errored = true;
            };
            errored = false;
        }

        public void Run()
        {
            try
            {
                Application.UseSystemConsole = true;
                Application.Init();

                var top = new Toplevel()
                {
                    X = Pos.At(0),
                    Y = Pos.At(0),
                    Width = Dim.Fill(),
                    Height = Dim.Fill()
                };
                top.Resized += (s) =>
                {
                    Application.Refresh();
                };
                var win = new Window("webgrep")
                {
                    X = Pos.Center(),
                    Y = Pos.Center(),
                    Width = Dim.Percent(75),
                    Height = Dim.Percent(75)
                };
                SetupWebgrepWindow(win);

                top.Add(win);
                Application.Run(top);
            }
            finally
            {
                Application.Shutdown();
            }
        }

        void SetupWebgrepWindow(View myView)
        {
            var label = new Label("Python:")
            {
                X = 1,
                Y = 1,
                Width = Dim.Fill() - 1,
                Height = 1
            };

            var editor = new TextView()
            {
                X = Pos.X(label),
                Y = Pos.Bottom(label),
                Width = Dim.Fill() - 1,
                Height = Dim.Fill() - 3
            };
            editor.ColorScheme = Colors.Dialog;
            editor.Autocomplete.AllSuggestions.Clear();
            AddInterfaceFunctions(editor);
            AddCommonVariables(editor);
            editor.Autocomplete.MaxWidth = 20;
            editor.WordWrap = true;

            var quit = new Button("_Quit")
            {
                X = Pos.X(label),
                Y = Pos.Bottom(editor) + 1,
                Width = 10,
                Height = 1
            };
            quit.Clicked += () => Application.RequestStop();
            var exec = new Button("_Execute")
            {
                X = Pos.AnchorEnd(1) - 12,
                Y = Pos.Bottom(editor) + 1,
                Width = 10,
                Height = 1
            };
            exec.Clicked += () => Execute(editor.Text.ToString());
            myView.Add(label, editor, quit, exec);
        }

        int cnt = 0;

        private void Execute(string cmds)
        {
            python.ExecuteString(cmds, $"cmds{cnt}");
            cnt++;

            if (browser.LastError != null)
            {
                sb.AppendLine($"[!] {browser.LastError}");
                errored = true;
            }
            browser.LastError = null;
            foreach (var log in browser.ErrorLog)
            {
                sb.AppendLine($"[#] {log}");
                errored = true;
            }
            browser.ErrorLog.Clear();

            var dialog = new Dialog()
            {
                Title = "Output",
                X = Pos.Center(),
                Y = Pos.Center(),
                Width = Dim.Percent(80),
                Height = Dim.Percent(80)
            };
            dialog.Resized += (s) =>
            {
                Application.Refresh();
            };
            if (errored)
                dialog.ColorScheme = Colors.Error;
            var editor = new TextView()
            {
                X = 0,
                Y = 0,
                Width = Dim.Fill(),
                Height = Dim.Fill() - 1
            };
            editor.ReadOnly = true;
            if (errored)
                editor.ColorScheme = Colors.Error;
            editor.Text = sb.ToString();
            editor.WordWrap = true;

            var btn = new Button()
            {
                Text = "_OK",
                X = Pos.Center(),
                Y = Pos.Bottom(editor),
                Width = 8,
                Height = 1
            };
            btn.Clicked += () => Application.RequestStop();
            if (errored)
                btn.ColorScheme = Colors.Error;

            sb.Clear();
            errored = false;
            dialog.Add(editor, btn);
            Application.Run(dialog);
        }

        private void AddCommonVariables(TextView script)
        {
            script.Autocomplete.AllSuggestions.AddRange(new[]
            {
                "browser",
                "params"
            });
        }

        private void AddInterfaceFunctions(TextView script)
        {
            var interfaces = typeof(TerminalUserInterfacePython).Assembly.DefinedTypes.Where(x => x.IsInterface && x.Namespace == "webgrep");
            var methods = interfaces.SelectMany(x => x.GetMethods().Where(m => !m.IsSpecialName).Select(x => x.Name));
            var properties = interfaces.SelectMany(x => x.GetProperties().Select(x => x.Name));
            var uniques = methods.Concat(properties).Distinct();
            script.Autocomplete.AllSuggestions.AddRange(uniques);
        }
    }
}