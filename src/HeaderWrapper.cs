using System;
using System.Collections.Generic;
using Microsoft.Playwright;

namespace webgrep
{
    public class HeaderWrapper
    {
        Header instance;

        public string Name
        {
            get
            {
                return instance.Name;
            }
            set
            {
                instance.Name = value;
            }
        }

        public string Value
        {
            get
            {
                return instance.Value;
            }
            set
            {
                instance.Value = value;
            }
        }

        public HeaderWrapper(Header header)
        {
            this.instance = header;
        }

        public HeaderWrapper(string name, string value)
        {
            this.instance = new Header { Name = name, Value = value };
        }

        public override bool Equals(object obj)
        {
            if (obj is HeaderWrapper)
            {
                HeaderWrapper tmp = (HeaderWrapper)obj;
                return this.Name == tmp.Name && this.Value == tmp.Value;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Value.GetHashCode();
        }
    }
}