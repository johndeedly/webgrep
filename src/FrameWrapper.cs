using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Playwright;
using toolbelt;

namespace webgrep
{
    public interface IFrameWrapper
    {
        IList<string> ErrorLog { get; }
        Exception LastError { get; set; }

        IEnumerable<IElementWrapper> AttachedElements(string selector);
        bool ClickOn(string selector, float? timeout = 5000);
        bool DblClickOn(string selector, float? timeout = 5000);
        bool ScrollIntoViewIfNeeded(string selector, float? timeout = 5000);
        string GetFormattedText(string selector, float? timeout = 5000);
        string GetText(string selector, float? timeout = 5000);
        string GetInnerHtml(string selector);
        string GetOuterHtml(string selector);
        string GetElementHtml(string selector);
        bool SetFileUpload(string selector, string path, float? timeout = 5000);
        bool SetFileUploads(string selector, IEnumerable<string> path, float? timeout = 5000);
        bool WaitDomContentLoaded(float? timeout = 5000);
        bool WaitUntilAttached(string selector, float? timeout = 5000);
        bool WaitUntilDetached(string selector, float? timeout = 5000);
    }

    public class FrameWrapper : IFrameWrapper
    {
        IFrame frame;
        IPageWrapper instance;

        public IList<string> ErrorLog => instance.ErrorLog;

        public Exception LastError
        {
            get => instance.LastError;
            set => instance.LastError = value;
        }

        public FrameWrapper(IFrame frame, IPageWrapper instance)
        {
            this.frame = frame;
            this.instance = instance;
        }

        public IEnumerable<IElementWrapper> AttachedElements(string selector)
        {
            try
            {
                return frame.QuerySelectorAllAsync(selector)
                    .Concat(x => x.Select(y => new ElementWrapper(y, this)))
                    .Await();
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return Enumerable.Empty<IElementWrapper>();
            }
        }

        public bool ClickOn(string selector, float? timeout = 5000)
        {
            try
            {
                frame.ClickAsync(selector, new FrameClickOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool DblClickOn(string selector, float? timeout = 5000)
        {
            try
            {
                frame.DblClickAsync(selector, new FrameDblClickOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool ScrollIntoViewIfNeeded(string selector, float? timeout = 5000)
        {
            try
            {
                frame.QuerySelectorAsync(selector)
                    .Concat(elem => new ElementWrapper(elem, this).ScrollIntoViewIfNeeded(timeout))
                    .Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public string GetFormattedText(string selector, float? timeout = 5000)
        {
            try
            {
                return frame.InnerTextAsync(selector, new FrameInnerTextOptions
                {
                    Timeout = timeout
                }).Concat(x => x.Trim()).Await();
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return string.Empty;
            }
        }

        public string GetText(string selector, float? timeout = 5000)
        {
            try
            {
                return frame.TextContentAsync(selector, new FrameTextContentOptions
                {
                    Timeout = timeout
                }).Concat(x => x.Trim()).Await();
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return string.Empty;
            }
        }

        public string GetInnerHtml(string selector)
        {
            try
            {
                return frame.QuerySelectorAsync(selector)
                    .Concat(elem => new ElementWrapper(elem, this).GetInnerHtml())
                    .Await();
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return string.Empty;
            }
        }

        public string GetOuterHtml(string selector)
        {
            try
            {
                return frame.QuerySelectorAsync(selector)
                    .Concat(elem => new ElementWrapper(elem, this).GetOuterHtml())
                    .Await();
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return string.Empty;
            }
        }

        public string GetElementHtml(string selector)
        {
            try
            {
                return frame.QuerySelectorAsync(selector)
                    .Concat(elem => new ElementWrapper(elem, this).GetElementHtml())
                    .Await();
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return string.Empty;
            }
        }

        public bool SetFileUpload(string selector, string path, float? timeout = 5000)
        {
            try
            {
                frame.SetInputFilesAsync(selector, path, new FrameSetInputFilesOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool SetFileUploads(string selector, IEnumerable<string> paths, float? timeout = 5000)
        {
            try
            {
                frame.SetInputFilesAsync(selector, paths, new FrameSetInputFilesOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool WaitDomContentLoaded(float? timeout = 5000)
        {
            try
            {
                frame.WaitForLoadStateAsync(LoadState.DOMContentLoaded, new FrameWaitForLoadStateOptions
                {
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                instance.ErrorLog.Add(ex.Message);
                instance.LastError = ex;
                return false;
            }
        }

        public bool WaitUntilAttached(string selector, float? timeout = 5000)
        {
            try
            {
                frame.WaitForSelectorAsync(selector, new FrameWaitForSelectorOptions
                {
                    State = WaitForSelectorState.Attached,
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool WaitUntilDetached(string selector, float? timeout = 5000)
        {
            try
            {
                frame.WaitForSelectorAsync(selector, new FrameWaitForSelectorOptions
                {
                    State = WaitForSelectorState.Detached,
                    Timeout = timeout
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }
    }
}