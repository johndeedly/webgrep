xml = {}

function xml.escape(s)
    if s == nil then
        return ''
    end
    s = s:gsub("&", "&amp;")
    s = s:gsub("<", "&lt;")
    s = s:gsub(">", "&gt;")
    s = s:gsub("'", "&apos;")
    s = s:gsub('"', "&quot;")
    return s
end

function sub_hex_ent(s) return schar(tonumber(s, 16)) end
function sub_dec_ent(s) return schar(tonumber(s)) end

function xml.unescape(s)
	if s == nil then
        return ''
    end
    s = s:gsub("&lt;", "<")
	s = s:gsub("&gt;", ">")
	s = s:gsub("&apos;", "'")
	s = s:gsub("&quot;", '"')
	s = s:gsub("&#x(%x+);", sub_hex_ent)
	s = s:gsub("&#(%d+);", sub_dec_ent)
	s = s:gsub("&amp;", "&")
	return s
end
