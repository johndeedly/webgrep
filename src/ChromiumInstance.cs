using Microsoft.Playwright;
using toolbelt;

namespace webgrep
{
    public sealed class ChromiumInstance : BrowserInstance
    {
        public ChromiumInstance(BrowserLaunchOptions options = null)
        {
            BrowserTypeLaunchOptions opts = null;
            if (options != null)
            {
                opts = new BrowserTypeLaunchOptions();
                if (options.Headless != null)
                    opts.Headless = options.Headless;
                if (options.ProxyServer != null)
                    opts.Proxy = new Proxy
                    {
                        Server = options.ProxyServer
                    };
            }
            BrowserNewContextOptions ctxopts = null;
            if (options?.AcceptDownloads != null)
            {
                ctxopts = new BrowserNewContextOptions()
                {
                    AcceptDownloads = options.AcceptDownloads
                };
            }
            browser = playwright.Chromium.LaunchAsync(opts).Await();
            context = browser.NewContextAsync(ctxopts).Await();
            NewPage();
        }
    }
}