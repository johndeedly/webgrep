namespace webgrep
{
    public class BrowserLaunchOptions
    {
        public bool? Headless { get; set; }
        public string ProxyServer { get; set; }
        public bool? AcceptDownloads { get; set; }
    }
}