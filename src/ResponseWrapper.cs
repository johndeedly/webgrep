using System;
using System.Collections.Generic;
using System.Text.Json;
using Microsoft.Playwright;
using toolbelt;

namespace webgrep
{
    public interface IResponseWrapper
    {
        IReadOnlyList<HeaderWrapper> Headers { get; }
        bool Ok { get; }
        int Status { get; }
        string StatusText { get; }
        byte[] Body { get; }
        JsonElement? Json { get; }
        string Text { get; }
        string Url { get; }
    }

    public class ResponseWrapper : IResponseWrapper
    {
        IAPIResponse response;
        IPageWrapper instance;
        
        public IReadOnlyList<HeaderWrapper> Headers
        {
            get
            {
#pragma warning disable 0612
                var headers = response.Headers;
#pragma warning restore 0612
                IList<HeaderWrapper> res = new List<HeaderWrapper>();
                foreach (var elem in headers)
                    res.Add(new HeaderWrapper(elem.Key, elem.Value));
                return (IReadOnlyList<HeaderWrapper>)res;
            }
        }

        public bool Ok => response.Ok;

        public int Status => response.Status;
        
        public string StatusText => response.StatusText;

        public byte[] Body => response.BodyAsync().Await();
        
        public JsonElement? Json => response.JsonAsync().Await();
        
        public string Text => response.TextAsync().Await();
        
        public string Url => response.Url;

        public ResponseWrapper(IAPIResponse response, IPageWrapper instance)
        {
            this.response = response;
            this.instance = instance;
        }
    }
}