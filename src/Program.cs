using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using toolbelt;
using webgrep.powershell;

namespace webgrep
{
    public class Program
    {
        enum BrowserType
        {
            Firefox,
            Chromium,
            Webkit
        }

        public static void Main(string[] args)
        {
            Logging.MinimumLoggingLevel = LoggingLevel.Information;
            Logging.ConsoleMinimumLevel = LoggingLevel.Information;
            Logging.WriteToFile = true;
            Logging.Initialize();

            bool headless = false;
            bool powershell = false;
            bool python = false;
            bool termui = false;
            BrowserType browserType = BrowserType.Firefox;
            string stdin = null;
            string[] parameters = null;
            List<string> scripts = new List<string>();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "--install" || args[i] == "-i")
                {
                    i++;
                    var exitCode = Microsoft.Playwright.Program.Main(new[] { "install", args[i] });
                    if (exitCode != 0)
                    {
                        Logging.Fatal($"failed to install browser {args[i]}");
                        return;
                    }
                    continue;
                }
                if (args[i] == "--powershell" || args[i] == "-s")
                {
                    powershell = true;
                    continue;
                }
                if (args[i] == "--python" || args[i] == "-n")
                {
                    python = true;
                    continue;
                }
                if (args[i] == "--headless" || args[i] == "-h")
                {
                    headless = true;
                    continue;
                }
                if (args[i] == "--termui" || args[i] == "-t")
                {
                    termui = true;
                    continue;
                }
                if (args[i] == "--firefox" || args[i] == "-f")
                {
                    browserType = BrowserType.Firefox;
                    continue;
                }
                if (args[i] == "--chromium" || args[i] == "-c")
                {
                    browserType = BrowserType.Chromium;
                    continue;
                }
                if (args[i] == "--webkit" || args[i] == "-w")
                {
                    browserType = BrowserType.Webkit;
                    continue;
                }
                if (args[i].StartsWith("-p"))
                {
                    parameters = args[i].Substring(2).Split(',').Select(x => x.Trim('"', '\'')).ToArray();
                    continue;
                }
                if (args[i].StartsWith("--param="))
                {
                    parameters = args[i].Substring(8).Split(',').Select(x => x.Trim('"', '\'')).ToArray();
                    continue;
                }
                if (args[i] == "-")
                {
                    using (StreamReader sr = new StreamReader(Console.OpenStandardInput()))
                    {
                        stdin = sr.ReadToEnd();
                    }
                    continue;
                }
                if (File.Exists(args[i]))
                {
                    scripts.Add(args[i]);
                    continue;
                }
                Logging.Error($"file not found: {args[i]}");
            }

            IBrowserInstance browser = null;
            try
            {
                switch (browserType)
                {
                    case BrowserType.Firefox:
                        browser = new FirefoxInstance(new BrowserLaunchOptions
                        {
                            Headless = headless
                        });
                        break;
                    case BrowserType.Chromium:
                        browser = new ChromiumInstance(new BrowserLaunchOptions
                        {
                            Headless = headless
                        });
                        break;
                    case BrowserType.Webkit:
                        browser = new WebkitInstance(new BrowserLaunchOptions
                        {
                            Headless = headless
                        });
                        break;
                    default:
                        throw new InvalidOperationException("unknown browser type");
                }
            }
            catch (Exception ex)
            {
                Logging.Fatal($"critical browser init error: {ex.Message}");
                return;
            }

            using (browser)
            {
                IScriptingWrapper script;
                if (!powershell && !python)
                {
                    // lua
                    script = ScriptingUtils.InitializeLua(false);
                    script.RegisterDelegateType(typeof(Action), typeof(NoInNoOutDelegate));
                    script.RegisterDelegateType(typeof(Action<IRouteWrapper>), typeof(OneInNoOutDelegate<IRouteWrapper>));
                    script.RegisterDelegateType(typeof(Func<string, bool>), typeof(OneInOneOutDelegate<string, bool>));

                    using (StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("webgrep.json.lua")))
                    {
                        string content = sr.ReadToEnd();
                        script.ExecuteString(content, "json.lua");
                    }
                    using (StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("webgrep.md5.lua")))
                    {
                        string content = sr.ReadToEnd();
                        script.ExecuteString(content, "md5.lua");
                    }
                    using (StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("webgrep.xml.lua")))
                    {
                        string content = sr.ReadToEnd();
                        script.ExecuteString(content, "xml.lua");
                    }
                }
                else if (!python)
                {
                    // powershell
                    script = ScriptingUtils.InitializePowerShell(false);
                    script.ExecuteString($"Import-Module -Name '{Assembly.GetEntryAssembly().Location}'");
                }
                else
                {
                    // python
                    script = ScriptingUtils.InitializePython(false);
                }
                
                using (script)
                {
                    script.SetVariable("browser", browser);
                    script.SetTable("params", parameters);
                    
                    if (stdin != null)
                    {
                        script.ExecuteString(stdin, "stdin");
                    }
                    else if (scripts.Any())
                    {
                        foreach (var file in scripts)
                        {
                            script.ExecuteFile(file);
                        }
                    }
                    else
                    {
                        if (!powershell && !python)
                            Logging.Error("no lua files given");
                        else if (!python)
                            Logging.Error("no powershell files given");
                        else
                            Logging.Error("no python files given");
                    }

                    if (termui)
                    {
                        if (!powershell && !python)
                        {
                            TerminalUserInterfaceLua inter = new TerminalUserInterfaceLua(script, browser);
                            inter.Run();
                        }
                        else if (!python)
                        {
                            TerminalUserInterfacePowerShell inter = new TerminalUserInterfacePowerShell(script, browser);
                            inter.Run();
                        }
                        else
                        {
                            TerminalUserInterfacePython inter = new TerminalUserInterfacePython(script, browser);
                            inter.Run();
                        }
                    }

                    if (browser.LastError != null)
                    {
                        Logging.Error($"[!] {browser.LastError}");
                    }
                    foreach (var log in browser.ErrorLog)
                    {
                        Logging.Error($"[#] {log}");
                    }
                }
            }
        }
    }
}