using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.Playwright;
using toolbelt;

namespace webgrep
{
    public interface IBrowserInstance : IPageWrapper, IDisposable
    {
        IReadOnlyList<IPageWrapper> Pages { get; }

        IPageWrapper NewPage();
    }

    public abstract class BrowserInstance : IBrowserInstance
    {
        protected IPlaywright playwright;
        protected IBrowser browser;
        protected IBrowserContext context;
        public IDictionary<string, Regex> routeCache;
        public IList<string> ErrorLog { get; private set; }
        public Exception LastError { get; set; }
        static readonly object _sync = new object();
        
        public IReadOnlyList<IPageWrapper> Pages
        {
            get
            {
                return context.Pages.Select(x => new PageWrapper(x, this)).ToArray();
            }
        }

        public IReadOnlyList<IFrameWrapper> Frames
        {
            get
            {
                return context.Pages[0].Frames.Select(x => new FrameWrapper(x, this)).ToArray();
            }
        }

        public BrowserInstance(BrowserLaunchOptions options = null)
        {
            playwright = Playwright.CreateAsync().Await();
            ErrorLog = new List<string>();
            routeCache = new Dictionary<string, Regex>();
        }

        public IPageWrapper NewPage()
        {
            var page = context.NewPageAsync().Await();
            return new PageWrapper(page, this);
        }

        public bool Route(string path, Action<IRouteWrapper> handler)
        {
            try
            {
                Regex rex;
                if (!routeCache.TryGetValue(path, out rex))
                {
                    rex = new Regex(path, RegexOptions.IgnoreCase);
                    routeCache[path] = rex;
                }
                context.RouteAsync(rex, (r) =>
                {
                    lock(_sync)
                    {
                        handler(new RouteWrapper(r, this));
                    }
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool Route(Func<string, bool> path, Action<IRouteWrapper> handler)
        {
            try
            {
                context.RouteAsync(path, (r) => 
                {
                    lock(_sync)
                    {
                        handler(new RouteWrapper(r, this));
                    }
                }).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool Unroute(string path)
        {
            try
            {
                Regex rex;
                if (routeCache.TryGetValue(path, out rex))
                {
                    context.UnrouteAsync(rex).Await();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool Unroute(Func<string, bool> path)
        {
            try
            {
                context.UnrouteAsync(path).Await();
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.Add(ex.Message);
                LastError = ex;
                return false;
            }
        }

        public bool ClosePage()
        {
            return new PageWrapper(context.Pages[0], this)
                .ClosePage();
        }

        public bool NavigateTo(string url, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .NavigateTo(url, timeout);
        }

        public bool NavigateToNoWait(string url, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .NavigateToNoWait(url, timeout);
        }

        public bool KeyboardTypeText(string text)
        {
            return new PageWrapper(context.Pages[0], this)
                .KeyboardTypeText(text);
        }

        public bool KeyboardPressKey(string key)
        {
            return new PageWrapper(context.Pages[0], this)
                .KeyboardPressKey(key);
        }

        public IEnumerable<IElementWrapper> AttachedElements(string selector)
        {
            return new PageWrapper(context.Pages[0], this)
                .AttachedElements(selector);
        }

        public bool ClickOn(string selector, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .ClickOn(selector, timeout);
        }

        public bool DblClickOn(string selector, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .DblClickOn(selector, timeout);
        }

        public bool ScrollIntoViewIfNeeded(string selector, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .ScrollIntoViewIfNeeded(selector, timeout);
        }

        public string GetFormattedText(string selector, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .GetFormattedText(selector, timeout);
        }

        public string GetText(string selector, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .GetText(selector, timeout);
        }

        public string GetInnerHtml(string selector)
        {
            return new PageWrapper(context.Pages[0], this)
                .GetInnerHtml(selector);
        }

        public string GetOuterHtml(string selector)
        {
            return new PageWrapper(context.Pages[0], this)
                .GetOuterHtml(selector);
        }

        public string GetElementHtml(string selector)
        {
            return new PageWrapper(context.Pages[0], this)
                .GetElementHtml(selector);
        }

        public bool SetFileUpload(string selector, string path, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .SetFileUpload(selector, path, timeout);
        }

        public bool SetFileUploads(string selector, IEnumerable<string> paths, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .SetFileUploads(selector, paths, timeout);
        }
        
        public bool TakeScreenshot(Stream stream, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .TakeScreenshot(stream, timeout);
        }
        
        public bool SaveScreenshot(string path, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .SaveScreenshot(path, timeout);
        }
        
        public bool TakeScreenPdf(Stream stream)
        {
            return new PageWrapper(context.Pages[0], this)
                .TakeScreenPdf(stream);
        }
        
        public bool SaveScreenPdf(string path)
        {
            return new PageWrapper(context.Pages[0], this)
                .SaveScreenPdf(path);
        }
        
        public bool TakePrintPdf(Stream stream)
        {
            return new PageWrapper(context.Pages[0], this)
                .TakePrintPdf(stream);
        }

        public bool SavePrintPdf(string path)
        {
            return new PageWrapper(context.Pages[0], this)
                .SavePrintPdf(path);
        }

        public JsonElement? Evaluate(string expression, params object[] arguments)
        {
            return new PageWrapper(context.Pages[0], this)
                .Evaluate(expression, arguments);
        }

        public bool WaitDomContentLoaded(float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .WaitDomContentLoaded(timeout);
        }

        public string DownloadSaveToPath(string path, Action actionsToStartDownload, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .DownloadSaveToPath(path, actionsToStartDownload, timeout);
        }

        public bool WaitUntilAttached(string selector, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .WaitUntilAttached(selector, timeout);
        }

        public bool WaitUntilDetached(string selector, float? timeout = 5000)
        {
            return new PageWrapper(context.Pages[0], this)
                .WaitUntilDetached(selector, timeout);
        }

        ~BrowserInstance()
        {
            Dispose();
        }

        public void Dispose()
        {
            context = null;
            browser?.CloseAsync().Await();
            browser = null;
            playwright?.Dispose();
            playwright = null;
        }
    }
}