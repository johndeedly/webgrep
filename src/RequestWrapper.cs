using System;
using System.Collections.Generic;
using Microsoft.Playwright;

namespace webgrep
{
    public interface IRequestWrapper
    {
        string Failure { get; }
        IReadOnlyList<HeaderWrapper> Headers { get; }
        bool IsNavigationRequest { get; }
        string Method { get; }
        string PostData { get; }
        byte[] PostDataBuffer { get; }
        IRequestWrapper RedirectedFrom { get; }
        IRequestWrapper RedirectedTo { get; }
        string ResourceType { get; }
        string Url { get; }
    }

    public class RequestWrapper : IRequestWrapper
    {
        IRequest request;
        IPageWrapper instance;
        
        public string Failure => request.Failure;

        public IReadOnlyList<HeaderWrapper> Headers
        {
            get
            {
#pragma warning disable 0612
                var headers = request.Headers;
#pragma warning restore 0612
                IList<HeaderWrapper> res = new List<HeaderWrapper>();
                foreach (var elem in headers)
                    res.Add(new HeaderWrapper(elem.Key, elem.Value));
                return (IReadOnlyList<HeaderWrapper>)res;
            }
        }
        
        public bool IsNavigationRequest => request.IsNavigationRequest;
        
        public string Method => request.Method;
        
        public string PostData => request.PostData;
        
        public byte[] PostDataBuffer => request.PostDataBuffer;
        
        public IRequestWrapper RedirectedFrom => request.RedirectedFrom == null ? new RequestWrapper(request.RedirectedFrom, instance) : null;
        
        public IRequestWrapper RedirectedTo => request.RedirectedTo == null ? new RequestWrapper(request.RedirectedTo, instance) : null;
        
        public string ResourceType => request.ResourceType;
        
        public string Url => request.Url;

        public RequestWrapper(IRequest request, IPageWrapper instance)
        {
            this.request = request;
            this.instance = instance;
        }
    }
}