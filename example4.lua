-- usage: --firefox example4.lua
filepath = browser:DownloadSaveToPath(nil, function()
    browser:NavigateToNoWait('https://www.blackhat.com/presentations/bh-dc-07/Sabanal_Yason/Paper/bh-dc-07-Sabanal_Yason-WP.pdf')
end)
print(filepath)