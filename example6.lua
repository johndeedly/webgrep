-- usage: --chromium example6.lua
if browser:NavigateTo('https://www.wikipedia.de/') then
    icon = dotnet.first(browser:AttachedElements('span.search-icon'))
    icon:SetAttribute('style', 'display:none;')
end