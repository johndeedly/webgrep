-- usage: --chromium example2.lua
if browser:NavigateTo('https://animalcrossingwiki.de/acnh/bastelanleitungen') then
    links = browser:AttachedElements('ul > li > div > a')
    for link in dotnet.each(links) do
        href = link:GetAttribute('href')
        page = browser:NewPage()
        if page:NavigateTo('https://animalcrossingwiki.de' .. href) then
            rows = page:AttachedElements('tr')
            for row in dotnet.each(rows) do
                cols = row:AttachedElements('td')
                name = dotnet.nth(cols, 2)
                if name ~= nil then
                    name = dotnet.first(name:AttachedElements('strong'))
                end
                place = dotnet.last(cols)
                -- CSV output
                if name ~= nil and place ~= nil then
                  print('"' .. link:GetText() .. '","' .. name:GetFormattedText() .. '","' .. place:GetText() .. '"')
                end
            end
        end
        page:ClosePage()
    end
end