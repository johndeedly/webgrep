-- usage: --chromium --headless example3.lua
if browser:NavigateTo('https://www.binary-zone.com/2021/07/19/windows-kernel-debugging-using-two-vms-on-linux/') then
    browser:SaveScreenPdf('windows-kernel-debugging-using-two-vms-on-linux.pdf')
end