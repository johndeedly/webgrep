# webgrep

<div align="center">

<a href="">[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]</a>
<a href="">![project status][status-shield]</a>

</div>

This work and the toolbelt project are licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

In addition you agree to the attached [disclaimer][disclaimer].

The project ["JSON4Lua"][JSON4Lua] by Craig Mason-Jones and Egor Skriptunoff is licensed under a MIT License.

The project ["md5.lua"][md5lua] by kikito is licensed under a MIT License.

[cc-by-sa]: LICENSE
[cc-by-sa-shield]: https://img.shields.io/badge/license-CC%20BY--SA%204.0-informational.svg
[disclaimer]: DISCLAIMER
[status-shield]: https://img.shields.io/badge/status-active%20development-brightgreen.svg
[JSON4Lua]: https://github.com/Egor-Skriptunoff/json4lua
[md5lua]: https://github.com/kikito/md5.lua

# NAME
webgrep - parse content from webpages, create pdf representations, navigate through web applications, download files: everything automated through the Lua, PowerShell or Python scripting languages

# SYNOPSIS
**webgrep** [*OPTIONS*] [*SCRIPTS*]

# OPTIONS
**\-\-powershell, -s**
: Run in PowerShell mode (executes *.ps1 scripts)

**\-\-python, -n**
: Run in Python mode (executes *.py scripts)

**\-\-termui, -t**
: Runs an interactive terminal gui after all scripts were executed to test commands or debug selectors

**\-\-install, -i**
: Install Playwright extensions (Chromium, Firefox, Webkit)

**\-\-headless, -h**
: Enable headless run

**\-\-firefox, -f**
: Use Firefox

**\-\-chromium, -c**
: Use Chromium/Chrome/Edge

**\-\-webkit, -w**
: Use Webkit

**\-\-param=[arg1,arg2], -p[arg1,arg2]**
: Define parameter values to use in the scripts - no brackets (first arg begins right after -p), comma separated, quotation marks at start and end will be trimmed. Access the parameter through the lua table "params[1..]"

**-**
: Read script file from stdin

# SCRIPTS
A chain of LUA, POWERSHELL or PYTHON script files executed in sequence

# LICENSE
Attribution-ShareAlike 4.0 International

# DESCRIPTION
As the title says

# IBrowserInstance implements IPageWrapper

**IReadOnlyList<IPageWrapper> Pages \[LUA,PY\], Get-Pages \[PS\]**
: List of open pages

**IReadOnlyList<IPageWrapper> Frames \[LUA,PY\], Get-Frames \[PS\]**
: List of open frames

**IPageWrapper NewPage() \[LUA,PY\], New-Page \[PS\]**
: Open new page in the same shared context as the other pages

**Start-Browser \[PS\]**
: Start new browser instance (not needed in interactive mode)

**Stop-Browser \[PS\]**
: Stops the given browser instance (not needed in interactive mode)


# IPageWrapper implements IFrameWrapper

**bool ClosePage() \[LUA,PY\], Close-Page \[PS\]**
: Closes the given page

**bool KeyboardPressKey(string key) \[LUA,PY\], Write-Keyboard \[PS\]**
: Simulates a keypress on the focused element

**bool KeyboardTypeText(string text) \[LUA,PY\], Write-Keyboard \[PS\]**
: Simulates entering text on the focused element

**bool NavigateTo(string url, float? timeout = 5000) \[LUA,PY\], Request-Navigation \[PS\]**
: Navigates to a new url

**bool NavigateToNoWait(string url, float? timeout = 5000) \[LUA,PY\], Request-Navigation \[PS\]**
: In combination with DownloadSaveToPath you can download a file to a specific path

**bool SaveScreenshot(string path, float? timeout = 5000) \[LUA,PY\], Save-Screenshot \[PS\]**
: Takes a screenshot from the whole page and saves it to disk

**bool SaveScreenPdf(string path) \[LUA,PY\], Save-ScreenPdf \[PS\]**
: Saves a PDF in the page's screen layout

**bool SavePrintPdf(string path) \[LUA,PY\], Save-PrintPdf \[PS\]**
: Saves a PDF in the page's print layout

**string DownloadSaveToPath(string path, Action actionsToStartDownload, float? timeout = 5000) \[LUA,PY\], Save-DownloadToPath \[PS\]**
: Start a download by performing the given actions

**JsonElement? Evaluate(string expression, params object[] arguments) \[LUA,PY\], Invoke-Javascript \[PS\]**
: Execute Javascript in the page's context


# IFrameWrapper

**IEnumerable<IElementWrapper> AttachedElements(string selector) \[LUA,PY\], Find-AttachedElements \[PS\]**
: Find all matching elements by CSS selector

**bool ClickOn(string selector, float? timeout = 5000) \[LUA,PY\], Invoke-Click \[PS\]**
: Click on any element matched by CSS selector

**bool DblClickOn(string selector, float? timeout = 5000) \[LUA,PY\], Invoke-Click \[PS\]**
: Doubleclick on any element matched by CSS selector

**bool ScrollIntoViewIfNeeded(string selector, float? timeout = 5000) \[LUA,PY\], Move-IntoView \[PS\]**
: Scrolls the viewport to make the element matched by CSS selector visible on screen

**string GetFormattedText(string selector, float? timeout = 5000) \[LUA,PY\], Get-Text \[PS\]**
: Retrieve the formatted text of any element matching the CSS selector

**string GetText(string selector, float? timeout = 5000) \[LUA,PY\], Get-Text \[PS\]**
: Retrieve the inner text of any element matching the CSS selector

**string GetInnerHtml(string selector) \[LUA,PY\], Get-InnerHtml \[PS\]**
: Retrieve the inner html content of the element matching the CSS selector

**string GetOuterHtml(string selector) \[LUA,PY\], Get-OuterHtml \[PS\]**
: Retrieve the inner html content of the element matching the CSS selector including it's own

**string GetElementHtml() \[LUA,PY\], Get-ElementHtml \[PS\]**
: Retrieve the html of the element itself matching the CSS selector including it's closing statement

**bool SetFileUpload(string selector, string path, float? timeout = 5000) \[LUA,PY\], Set-FileUpload \[PS\]**
: Set the file upload element by CSS selector to the given path

**bool SetFileUploads(string selector, IEnumerable<string> path, float? timeout = 5000) \[LUA,PY\], Set-FileUpload \[PS\]**
: Set the file upload element by CSS selector to all the given paths

**bool WaitDomContentLoaded(float? timeout = 5000) \[LUA,PY\], Wait-DomContentLoaded \[PS\]**
: Wait until all DOM content is loaded on the page

**bool WaitUntilAttached(string selector, float? timeout = 5000) \[LUA,PY\], Wait-UntilAttached \[PS\]**
: Wait until an element is attached to the DOM

**bool WaitUntilDetached(string selector, float? timeout = 5000) \[LUA,PY\], Wait-UntilDetached \[PS\]**
: Wait until an element is detached from the DOM


# IElementWrapper

**IEnumerable<IElementWrapper> AttachedElements(string selector) \[LUA,PY\], Find-AttachedElements \[PS\]**
: Find all matching elements by CSS selector below the given root element

**bool ClickOn(float? timeout = 5000) \[LUA,PY\], Invoke-Click \[PS\]**
: Clicks on the given element

**bool DblClickOn(float? timeout = 5000) \[LUA,PY\], Invoke-Click \[PS\]**
: Doubleclicks on the given element

**bool ScrollIntoViewIfNeeded(float? timeout = 5000) \[LUA,PY\], Move-IntoView \[PS\]**
: Scrolls the viewport to make the element visible on screen

**string GetFormattedText() \[LUA,PY\], Get-Text \[PS\]**
: Retrieve the formatted text of the element

**string GetText() \[LUA,PY\], Get-Text \[PS\]**
: Retrieve the inner text of the element

**string GetInnerHtml() \[LUA,PY\], Get-InnerHtml \[PS\]**
: Retrieve the inner html content of the element

**string GetOuterHtml() \[LUA,PY\], Get-OuterHtml \[PS\]**
: Retrieve the inner html content of the element including it's own

**string GetElementHtml() \[LUA,PY\], Get-ElementHtml \[PS\]**
: Retrieve the html of the element itself including it's closing statement

**string GetAttribute(string name) \[LUA,PY\], Get-Attribute \[PS\]**
: Retrieves the attribute value on the given element

**string SetAttribute(string name, string value) \[LUA,PY\], Set-Attribute \[PS\]**
: Set the attribute value on the given element

**bool SetFileUpload(string path, float? timeout = 5000) \[LUA,PY\], Set-FileUpload \[PS\]**
: Set the file upload on the element

**bool SetFileUploads(IEnumerable<string> path, float? timeout = 5000) \[LUA,PY\], Set-FileUpload \[PS\]**
: Set all file uploads on the element

**bool SaveScreenshot(string path, float? timeout = 5000) \[LUA,PY\], Save-Screenshot \[PS\]**
: Saves a screenshot of the given element to path

**bool WaitUntilAttached(string selector, float? timeout = 5000) \[LUA,PY\], Wait-UntilAttached \[PS\]**
: Wait until the selected element(s) below the given one is/are attached to the DOM

**bool WaitUntilDetached(string selector, float? timeout = 5000) \[LUA,PY\], Wait-UntilDetached \[PS\]**
: Wait until the selected element(s) below the given one is/are attached to the DOM

**JsonElement? Evaluate(string expression, params object[] arguments) \[LUA,PY\], Invoke-Javascript \[PS\]**
: Execute Javascript in the element's context

# browser implements IBrowserInstance

**browser \[LUA, PS\]**
: Represents the wrapper object around the C# logic for accessing the browsers

# dotnet

**dotnet.any(o)**
: Returns true if there are any elements contained inside the dotnet IEnumerable "o"
```
x = dotnet.any(o)
```

**dotnet.concat(o1, o2)**
: Iterates over each dotnet IEnumerable element in "o1", then in "o2"
```
for x in dotnet.concat(o1, o2) do
    x:foo()
end
```

**dotnet.each(o)**
: Iterates over each dotnet IEnumerable element in "o"
```
for x in dotnet.each(o) do
    x:foo()
end
```

**dotnet.first(o)**
: Returns the first dotnet IEnumerable element in "o"
```
x = dotnet.first(o)
```

**dotnet.nth(o, n)**
: Returns the nth dotnet IEnumerable element in "o" (n ≥ 1)
```
x = dotnet.nth(o, n)
```

**dotnet.last(o)**
: Returns the last dotnet IEnumerable element in "o"
```
x = dotnet.last(o)
```

**dotnet.zip(o1, o2)**
: Iterates over each dotnet IEnumerable element in "o1" and "o2" and passes each corresponding element with the same numbered index to the function
```
for x1, x2 in dotnet.zip(o1, o2) do
    x1:foo()
    x2:bar()
end
```

**dotnet.count(o)**
: Returns the count of dotnet IEnumerable elements in "o"
```
cnt = dotnet.count(o)
```

# json

**json.decode(s)**
: Converts a json string to a lua table
```
tbl = json.decode(str)
```

**json.encode(t)**
: Converts a lua table to a json string
```
str = json.encode(tbl)
```

# EXAMPLE #1: Counting div elements on Google's page
\[LUA\]
```
if browser:NavigateTo('https://www.google.com/') then
    divs = browser:AttachedElements('div')
    i = 0
    for x in dotnet.each(divs) do
        i = i + 1
    end
    print('Element count: ' .. i)
end
```
\[POWERSHELL\]
```
Request-Navigation -Browser $browser -Url "http://www.google.com"

$divs = Find-AttachedElements -Browser $browser -Selector "div"
Write-Host "Number of divs on page: $($divs.Count)"

```
\[PYTHON\]
```
if browser.NavigateTo('https://www.google.com/'):
    divs = browser.AttachedElements('div')
    i = sum(1 for _ in divs)
    print('Number of divs on page: ' + str(i))
```

# EXAMPLE #2: Parse all the items of the game Animal Crossing and write them out to a CSV table
\[LUA\]
```
if browser:NavigateTo('https://animalcrossingwiki.de/acnh/bastelanleitungen') then
    links = browser:AttachedElements('ul > li > div > a')
    for link in dotnet.each(links) do
        href = link:GetAttribute('href')
        page = browser:NewPage()
        if page:NavigateTo('https://animalcrossingwiki.de' .. href) then
            names = page:AttachedElements('td.col1 > strong')
            places = page:AttachedElements('td.col4')
            for name, place in dotnet.zip(names, places) do
                -- CSV output
                print('"' .. link:GetText() .. '","' .. name:GetText() .. '","' .. place:GetText() .. '"')
            end
        end
        page:ClosePage()
    end
end
```

# EXAMPLE #3: Save a screen layout pdf of a webpage
\[LUA\]
```
if browser:NavigateTo('https://www.binary-zone.com/2021/07/19/windows-kernel-debugging-using-two-vms-on-linux/') then
    browser:SaveScreenPdf('windows-kernel-debugging-using-two-vms-on-linux.pdf')
end
```

# EXAMPLE #4: Download a file
\[LUA\]
```
filepath = browser:DownloadSaveToPath(nil, function()
    browser:NavigateToNoWait('https://www.blackhat.com/presentations/bh-dc-07/Sabanal_Yason/Paper/bh-dc-07-Sabanal_Yason-WP.pdf')
end)
print(filepath)
```

# EXAMPLE #5: An example for bash command line parameters
\[LUA\]
```
-- webgrep -p2,3 example5.lua
print(params[1])
print("+")
print(params[2])
print("=")
res = params[1] + params[2]
print(res)
```
