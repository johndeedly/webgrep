-- usage: --chromium example7.lua
browser:Route('.', function(rt)
    -- disable caching
    if dotnet.any(rt.Request.Headers) then
        print('=== ' .. rt.Request.Url .. ' ===')
        for x in dotnet.each(rt.Request.Headers) do
            print('  | ' .. x.Name .. ' = [' .. x.Value .. ']')
        end
    end
    rt:Continue()
end)
browser:Route(function(url)
    return url:find('tracking.js') ~= nil
end, function(rt)
    print('### blocked javascript ' .. rt.Request.Url)
    rt:Fetch(function(rs)
        text = rs.Text
        print(text)
        rt:Abort()
    end)
end)
browser:Route(function(url)
    return url:find('.png') ~= nil or url:find('.svg') ~= nil or url:find('.gif') ~= nil or url:find('.jpg') ~= nil or url:find('.jpeg') ~= nil
end, function(rt)
    print('### blocked image ' .. rt.Request.Url)
    rt:Abort()
end)
if browser:NavigateTo('https://www.wikipedia.de/') then
    os.execute('sleep 3')
end