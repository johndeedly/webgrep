if browser.NavigateTo('https://www.google.com/'):
    divs = browser.AttachedElements('div')
    i = sum(1 for _ in divs)
    print('Number of divs on page: ' + str(i))
